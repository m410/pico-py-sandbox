#
# tap 1: level 1 start slow & dim
# tap 2: level 2 faster and brighter
# tap 3: stop
#
#
# Future update, add low power mode
#  https://github.com/tomjorquera/pico-micropython-lowpower-workaround
#
import _thread
import machine
import time
import random

LOOP_PAUSE = 0.005
PWN_FREQ = 500
MIN_DUTY = 0
MAX_DUTY = 10_000
PIN_IDS = [0, 1, 2, 3, 4, 5, 6]
STOP = False
LIGHTS = []
LIGHT_MODE = 1

class LightState:
    def __init__(self, timestamp, delay, speed):
        self.timestamp = timestamp
        self.brightness = 1
        self.grow = True
        self.delay = delay
        self.speed = speed
        
    def __str__(self) -> str:
        return "%s(%r)" % (self.__class__, self.__dict__)


class Light:
    def __init__(self, pin: int, brightness: int, speed: int, running: bool):
        self.pin = pin
        self.lightState = None;
    
    def init_state(self, timestamp, delaySeed, speed):
        delay = 1000 * delaySeed
        self.lightState = LightState(timestamp, delay, speed);
        return self.lightState

    def get_state(self):
        return self.lightState
    
    def set_state(self, state):
        self.lightState = state
        
#
# light movement on core1
#
def run_light():    
    while not STOP:        
        currentTimestamp = time.ticks_ms()
            
        for light in LIGHTS:
            state = light.get_state()
            
            if(LIGHT_MODE == 0):
                pin = machine.Pin(light.pin)
                pwm = machine.PWM(machine.Pin(light.pin))
                pwm.freq(PWN_FREQ)
                pwm.duty_u16(0)
                light.set_state(None)
            
            else:    
                if (state is None):
                    delay = random.randrange(8)
                    speed = random.randrange(2,40)
                    state = light.init_state(currentTimestamp, delay, speed)
                
                if state.delay <= (currentTimestamp - state.timestamp): 
                    
                    if state.brightness >= MAX_DUTY:
                        state.grow = False
                        
                    elif state.brightness <= MIN_DUTY :
                        light.set_state(None)
                    
                    if state.brightness < MAX_DUTY and state.grow: 
                        state.brightness = state.brightness + state.speed
                    
                    elif state.brightness > MIN_DUTY and not state.grow:  
                        state.brightness = state.brightness - state.speed
                    
                    if state.brightness  > MIN_DUTY and state.brightness < MAX_DUTY: 
                        pwm = machine.PWM(machine.Pin(light.pin))
                        pwm.freq(PWN_FREQ)
                        pwm.duty_u16(state.brightness)
                    
        time.sleep(LOOP_PAUSE)  # Adjust the delay for fading speed

#
# listen to button or vibration on core0
#
def run_main():
    global LIGHT_MODE
    adc = machine.ADC(machine.Pin(28))
    VIBRATION_THRESHOLD = 400
    SAMPLE_SIZE = 20

    while not STOP:
        sample_group = []
    
        for x in range(SAMPLE_SIZE):
            sample = adc.read_u16()
            sample_group.append(sample)
            time.sleep(.01)
        
        # Get rid of the outliers
        sample_group.remove(max(sample_group))
        sample_group.remove(min(sample_group))
        
        # Get the average
        vibration = sum(sample_group)/ len(sample_group)
        # vibration = adc.read_u16()
        
        if(vibration > VIBRATION_THRESHOLD):
            LIGHT_MODE = 0 if LIGHT_MODE > 0 else 1
            STATE = "ON" if LIGHT_MODE > 0 else "OFF"
            print(f"Vibrate {STATE} ({vibration})")
            
        time.sleep(0.05)

#
# Setup initial Light States
#
for pin in PIN_IDS:
    LIGHTS.append(Light(pin, 1, 2, False))

# low power
# https://github.com/tomjorquera/pico-micropython-lowpower-workaround


try:
    _thread.start_new_thread(run_light, ())
    run_main()
except KeyboardInterrupt:
    print('Got ctrl-c')
    STOP = True
    
    for light in LIGHTS:
        pin = machine.Pin(light.pin)
        pwm = machine.PWM(machine.Pin(light.pin))
        pwm.freq(PWN_FREQ)
        pwm.duty_u16(0)



