#
# this works
# 
from machine import Pin
import time

#
# https://ohmslawcalculator.com/led-resistor-calculator
# source 5V
#
# Red LED: 2V 15mA == 200
# Green LED: 2.1V 20mA == 125
# Blue LED: 3.2V 25mA == 72
# White LED: 3.2V 25mA == 72

STOP = False
PIN_IDS = [2,3,4,5,6,7,8]
PINS = []

try:
    # Q: can this be swapped for 25?
    PINS.append(Pin("LED", Pin.OUT))
    PINS.extend(Pin(p, Pin.OUT) for p in PIN_IDS)
    
    while not STOP:
        led = Pin("LED", Pin.OUT)
        led.on()
        
        for p in PINS:
            p.on()
            
        time.sleep(0.5)
        
        # off
        for p in PINS:
            p.off()
            
        time.sleep(0.5)
    
except KeyboardInterrupt:
    print('Got ctrl-c')
    STOP = True
    for p in PINS:
        p.off()
