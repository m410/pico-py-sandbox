import random
import signal
import threading
import time

exit_event = threading.Event()


def bg_thread1():
    for i in range(1, 10):
        print(f'thread1 {i} of 10 iterations...')
        time.sleep(random.randint(0,5))  # do some work...

        if exit_event.is_set():
            break

    print(f'thread1 {i} iterations completed before exiting.')

def bg_thread2():
    for i in range(1, 10):
        print(f'thread2 {i} of 10 iterations...')
        time.sleep(random.randint(0,5))  # do some work...

        if exit_event.is_set():
            break

    print(f'thread2 {i} iterations completed before exiting.')
    

def signal_handler(signum, frame):
    exit_event.set()


signal.signal(signal.SIGINT, signal_handler)
th1 = threading.Thread(target=bg_thread1, daemon=True, name='BG Thread 1')
th1.start()

th2 = threading.Thread(target=bg_thread2, daemon=True, name='BG Thread 2')
th2.start()

th1.join()
th2.join()