import machine

button_pin = machine.Pin(14, machine.Pin.IN, machine.Pin.PULL_UP)

while True:
    # Wait for the button to be pressed
    while button_pin.value() == 1:
        machine.sleep(100)
    
    # Put the board into deep sleep mode for 5 seconds
    machine.deepsleep(5000)
