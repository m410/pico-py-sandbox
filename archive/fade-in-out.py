#
# this works
#
import machine
import time

# Define the GPIO pin number for the LED
led_pin = 4

# Create a PWM object with a frequency of 500 Hz
pwm = machine.PWM(machine.Pin(led_pin))
pwm.freq(500)

# Define the duty cycle range for fading in and out
min_duty = 0
max_duty = 1023
STOP = False

        
try:
    # Fade in and out loop
    while not STOP:
        # Fade in
        for duty in range(min_duty, max_duty + 1):
            print('in')
            pwm.duty_u16(duty)
            time.sleep(0.005)  # Adjust the delay for fading speed

        # Fade out
        for duty in range(max_duty, min_duty - 1, -1):
            print('out')
            pwm.duty_u16(duty)
            time.sleep(0.005)  # Adjust the delay for fading speed

except KeyboardInterrupt:
    print('Got ctrl-c')
    STOP = True