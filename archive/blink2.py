#
# this works
#
from machine import Pin, Timer

led = Pin(16, Pin.OUT)
tim = Timer()
count = 1

def tick(timer):
    global led
    led.toggle()
    print("count ")

tim.init(freq=2.5, mode=Timer.PERIODIC, callback=tick)
