import network
import socket
import time

class WebServer():
    port = 80
    ssid
    password
    
    def __init__(self, ssid, password):
        print('server init')
        self.ssid = ssid
        self.password = password


    def addHandler(handler):
        print('add handler', handler)


    def start():
        print('start')
        wlan = network.WLAN(network.STA_IF)
        wlan.active(True)
        wlan.connect(ssid, password)
        addr = socket.getaddrinfo('0.0.0.0', 80)[0][-1]
        s = socket.socket()
        s.bind(addr)
        s.listen(1)
        
        while True:
            try:
                cl, addr = s.accept()
                print('client connected from', addr)
                request = cl.recv(1024)
                request = str(request)
                cl.send('HTTP/1.0 200 OK\r\nContent-type: text/html\r\n\r\n')
                cl.send(response)
                cl.close()
            except OSError as e:
                cl.close()
                print('connection closed')

    def stop():
        print('start')