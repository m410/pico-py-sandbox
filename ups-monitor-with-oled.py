# https://github.com/geeekpi/upsplus
import machine
import time
from ssd1306 import SSD1306_I2C

WIDTH = 128
HEIGHT = 64

txPin = machine.Pin(0)
rxPin = machine.Pin(1)
sdaPin = machine.Pin(2)
sclPin = machine.Pin(3)

i2c = machine.I2C(1, scl=sclPin, sda=sdaPin, freq=200_000)
oled = SSD1306_I2C(WIDTH, HEIGHT, i2c)


upsUart = machine.UART(0, baudrate=11520, rx=rxPin, tx=txPin)
STOP = False

try:
    while not STOP:
        data = upsUart.readline()
    
        if data != None:
            decode = data.decode('utf-8')
            normal = decode.rstrip('\n')
            
            if '|' in normal:        
                print("Battery Voltage: {}mV".format(normal.split('|')[0]))
                oled.text("BV: {}mV".format(normal.split('|')[0]), 0, 10)
                print("Current Charing in: {}mA".format(normal.split('|')[1]))
                oled.text("CRH: {}mA".format(normal.split('|')[1]),0,20)
                battery_current = float(normal.split('|')[2] / 10.0 )
                print("Consuming Current: {}mA".format(battery_current))
                oled.text("Consuming Current: {}mA".format(battery_current), 0,30)
        
                sign = normal.split('|')[2]
                print("sigh: {}".format(sign))
            
                if float(sign) < 0.0:
                    print("Battery is Charging...")
                    oled.text("Bt: Charging", 0, 40)
                else:
                    print("Power is consuming...")
                    oled.text("Bt: Consuming", 0, 40)
            else:
                print("Empty: {}".format(normal))
                oled.text("Empty",0,10)
        else:
            print("No Data")
            oled.text("No Data",0,10)
        
        oled.show()
        time.sleep(2)
        print("-"*38)
        
except ValueError:
    print("Value Error: {}".format(ValueError))
    STOP = True
    
except IndexError:
    print("Index Error: {}".format(IndexError))
    STOP = True
    
except KeyboardInterrupt:
    print('Got ctrl-c')
    STOP = True